console.log("s19");

// 3. Create a variable getCube and use the exponent 
//    operator to compute the cube of a number. (A cube is any number raised to 3)

let number = [2];

let cubeNumbers = number.map(number => number ** 3)
	// implicit return statement
// 4. Using Template Literals, print out the value of the 
//    getCube variable with a message of The cube of <num> is…
	let message = "The cube of " + number + " is " + cubeNumbers;
console.log(message);

// ============================================================

const completeAddress = ["258 Washington Ave NW, ", "California", "90011"];
// 5. Create a variable address with a value of an array containing details of an address.
const [streetName, state, zipCode] = completeAddress;
// console.log(streetName); // // index 0
// console.log(state); // // index 1
// console.log(zipCode); // // index 2
// 6. Destructure the array and print out a message with the full address using Template Literals.

// hello Juan Dela Cruz it's nice to meet you.
console.log(`I live at ${streetName} ${state} ${zipCode}`)

// ============================================================
// 7. Create a variable animal with a value of 
//    an object data type with different animal details as it’s properties.
const animal = {
	name: "Lolong",
	type: "saltwater",
	KindOfAnimal: "Crocodile",
	weight: 1075,
	height: 20,
	inches: 3
};
const {name,type, KindOfAnimal, weight, height,inches} = animal;
		console.log(`${name} was a ${type} animal. He weighed at ${weight} kg with a 
measurement of ${weight} ft ${inches} inch.`);

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

// 9. Create an array of numbers.

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
let numbers = [1,2,3,4,5]
		numbers.forEach((number) => {
			console.log(number);
})
// let variableName = arrayName.reduce((acc, cur) => acc + cur);
// let reduceNumber = numbers.reduce((acc, cur) => acc + cur){
// 			return num += num;
// }
	let reduceNumber = numbers.reduce(function(num){
			return num += num;
})
console.log(reduceNumber);
// Creating Class
// class Car{
// 	constructor(brand, name, year){
// 		this.brand = brand;
// 		this.name = name;
// 		this.year = year;
// 	}
// }
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

const myDog = new Dog();
		myDog.name = "Frankie";
		myDog.age = 5;
		myDog.breed = "Miniature Dachshund";

console.log(myDog);

// Instantiating an object
// "new" operator which create/instantiates a new object with the given arguments as the value of the property.
// let/const variableName = new className

// const it does not mean that it's properties cannot be change.
// const myCar = new Car();

// console.log(myCar);

// // Initialized/assigend a value in our object
// myCar.brand = "Ford";
// myCar.name = "Ranger Raptor";
// myCar.year = 2021;
	
// // we can't change the variable to another data type or store another value.
// // myCar = "Ford Ranger Raptor 2021";

// console.log(myCar);

// // Creating an object with initialization of values
// const myNewCar = new Car("Toyota", "Vios", 2021);
// myNewCar.year = 2022;

// console.log(myNewCar);

// // ==========================================================

// const students = ["john", "jane", "judy"];
// console.log(students);

// students.forEach((student) => {
// 	console.log(`${student} is a student`);
// })
// // ==========================================================
















